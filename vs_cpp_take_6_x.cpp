#include <iostream>
#include "Stack.h"

int main()
{
    //���� int:
    std::cout << "----- ������������ � ������� INT -----\n";
    int* intArr = new int[3];
    *intArr = 1;
    *(intArr + 1) = 2;
    *(intArr + 2) = 3;
    Stack<int>* intStack = new Stack<int>(intArr, 3);
    intStack->print();
    intStack->push(7);
    std::cout << "���孨� �������: " << *(intStack->top()) << "\n";
    std::cout << "\t - ��������� ���孥�� ������� �� 8...\n";
    *(intStack->top()) = 8;
    std::cout << "���孨� �������: " << *(intStack->top()) << "\n";
    intStack->push(2);
    intStack->push(3);
    intStack->print();
    std::cout << "����� ��⠢��� " << intStack->getLength() << "\n";
    std::cout << "���孨� �������: " << *(intStack->top()) << "\n";
    intStack->pop();
    intStack->pop();
    intStack->pop();
    intStack->print();
    std::cout << "����� ��⠢��� " << intStack->getLength() << "\n";
    std::cout << "���孨� �������: " << *(intStack->top()) << "\n";

    //Test with string:
    std::cout << "\n----- ������������ � ������� STRING -----\n";
    std::string* stringArr = new std::string[3];
    *stringArr = "��� �������";
    *(stringArr + 1) = "���� ������";
    *(stringArr + 2) = "��ࣥ� ������";
    Stack<std::string>* stringStack = new Stack<std::string>(stringArr, 3);
    stringStack->print();
    stringStack->push("����਩ �������");
    std::cout << "���孨� �������: " << *(stringStack->top()) << "\n";
    std::cout << "\t - ��������� ���孥�� ������� �� �������� ��堩���...\n";
    *(stringStack->top()) = "�������� ��堩���";
    std::cout << "���孨� �������: " << *(stringStack->top()) << "\n";
    stringStack->push("����ᠭ�� �૮�");
    stringStack->push("����⠭⨭ �������஢");
    stringStack->print();
    std::cout << "����� ��⠢��� " << stringStack->getLength() << "\n";
    std::cout << "���孨� �������: " << *(stringStack->top()) << "\n";
    stringStack->pop();
    stringStack->pop();
    stringStack->pop();
    stringStack->print();
    std::cout << "����� ��⠢��� " << stringStack->getLength() << "\n";
    std::cout << "���孨� �������: " << *(stringStack->top()) << "\n";

    return 0;
}