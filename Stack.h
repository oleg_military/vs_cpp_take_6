#pragma once
#pragma once
#include <iostream>

/**
* @brief �⥪ ������⮢ ⨯� <T>.
*/
template<typename T>
class Stack
{
public:
	Stack() : length(0), stack(nullptr), topPointer(nullptr) {};

	Stack(T* array, size_t arrayLength) : stack(array),
		length(arrayLength),
		topPointer(array + arrayLength - 1)
	{
		std::cout << "��� �⥪ <" << typeid(T).name() << "> ᮧ������.\n";
	};
	size_t getLength() const;
	void pop();
	void push(T element);
	T* top();
	void print();

private:
	T* stack{ nullptr };
	size_t length{ 0 };
	T* topPointer{ nullptr };
};

/**
*@brief ����祭�� ����� �⥪.
*@return ����� �⥪
*/
template<typename T>
inline size_t Stack<T>::getLength() const
{
	return length;
}

/**
*@brief ��������� (㤠����) ������� �� ���孥� ��� �⥪�.
*/
template<typename T>
inline void Stack<T>::pop()
{
	if (length == 0 || topPointer == nullptr)
	{
		std::cerr << "��� ������⮢ � �⥪�.\n";
	}
	else if (length == 1)
	{
		std::cout << "\t - �������� ������� " << *topPointer << "\n";
		delete[] stack;
		stack = nullptr;
		topPointer = nullptr;
		length = 0;
	}
	else
	{
		std::cout << "\t - �������� ������� " << *topPointer << "\n";
		--length;
		T* resizedStack = new T[length];
		for (size_t i = 0; i < length; ++i)
		{
			*(resizedStack + i) = *(stack + i);
		}
		delete[] stack;
		stack = resizedStack;
		topPointer = stack + length - 1;
	}
}

/**
*@brief ��६���� ������� � �⥪.
*@param [in] ������� - �������, �������騩 ��⠫�������
*/
template<typename T>
inline void Stack<T>::push(T element)
{
	std::cout << "\t - ���������� ������� " << element << "\n";
	T* resizedStack = new T[length + 1];
	for (size_t i = 0; i < length; ++i)
	{
		*(resizedStack + i) = *(stack + i);
	}
	delete[] stack;
	stack = resizedStack;
	*(stack + length) = element;
	topPointer = stack + length;
	++length;
}

/**
*@brief ����祭�� 㪠��⥫� �� ���設� �⥪�.
*@return T* �����
*/
template<typename T>
inline T* Stack<T>::top()
{
	return topPointer;
}

/**
*@brief �뢥��� �������� �⥪� � ���᮫�.
*/
template<typename T>
void Stack<T>::print()
{
	for (size_t i = 0; i < length; ++i)
	{
		std::cout << "\'" << *(stack + i) << "\'" << " ";
	}
	std::cout << "\n";
}